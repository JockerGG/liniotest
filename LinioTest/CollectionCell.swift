//
//  CollectionCell.swift
//  LinioTest
//
//  Created by Eduardo García González on 12/16/18.
//  Copyright © 2018 Eduardo García González. All rights reserved.
//

import UIKit

class CollectionCell: UICollectionViewCell {
    @IBOutlet var images: [UIImageView]!
    @IBOutlet weak var collectionName: UILabel!
    @IBOutlet weak var productsCount: UILabel!
    @IBOutlet weak var nameContainer: UIView!
    
    override func awakeFromNib() {
        collectionName.text = ""
        productsCount.text = ""
        productsCount.textColor = .softText
        images.forEach { (image) in
            image.image = UIImage(named: "ndIc48LikeOffWhite")?.withRenderingMode(.alwaysTemplate)
            image.tintColor = UIColor.purple
            image.setViewCornerRadius(radius: 7)
            image.backgroundColor = .white
        }
        nameContainer.setViewCornerRadius(radius: 7)
    }
    
    func configureWith(model : Collection){
        collectionName.text = model.name
        productsCount.text = model.products.count.description
        getImages(of: model.products)
    }
    
    private func getImages(of products : [String : Product]){
        var imagesToLoad : [UIImage] = []
        for object in products{
            if let url = URL(string: object.value.image) {
                if let data = try? Data(contentsOf: url) {
                    if let image = UIImage(data: data) {
                        imagesToLoad.append(image)
                    }
                }
            }
            if(imagesToLoad.count == 3){
                break
            }
        }
        
        for (index, image) in imagesToLoad.enumerated(){
            images[index].image = image
        }
    }
    
    override func prepareForReuse() {
        collectionName.text = ""
        productsCount.text = ""
        images.forEach { (image) in
            image.image = UIImage(named: "ndIc48LikeOffWhite")?.withRenderingMode(.alwaysTemplate)
            image.tintColor = UIColor.purple
        }
    }
    
}

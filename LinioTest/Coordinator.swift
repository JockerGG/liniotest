//
//  Coordinator.swift
//  LinioTest
//
//  Created by Eduardo García González on 12/15/18.
//  Copyright © 2018 Eduardo García González. All rights reserved.
//

import Foundation
import UIKit

protocol Coordinator {
    var navigationController: UINavigationController { get set }
    
    /**
        This function is necessary to start a view controller with the coordinator pattern
    */
    func start()
}

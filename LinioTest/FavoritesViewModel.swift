//
//  FavoritesViewModel.swift
//  LinioTest
//
//  Created by Eduardo García González on 12/16/18.
//  Copyright © 2018 Eduardo García González. All rights reserved.
//

import UIKit
typealias FavoritesViewModelDelegate = BaseViewModel
typealias Collections = [Collection]

class FavoritesViewModel: NSObject {
    
    var delegate : FavoritesViewModelDelegate?
    
    var alertModel : AlertModel?{
        didSet{
            guard let model = alertModel else {return}
            delegate?.showAlert(with: model)
        }
    }
    
    var isLoading : Bool = false {
        didSet{
            delegate?.updateLoader(isLoading: isLoading)
        }
    }
    
    lazy var model = {
        return FavoritesModel(data: [], products: [])
    }()
    
    func readAndDecodeJSON(){
        isLoading = true
        model.data.removeAll()
        model.products.removeAll()
        delegate?.reloadData()
        guard let path = Bundle.main.path(forResource: "local", ofType: "json") else {
            isLoading = false
            showJSONError()
            return
        }
        guard let data = try? Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe) else {
            isLoading = false
            showJSONError()
            return
        }
        
        guard let response = try? JSONDecoder().decode(Collections.self, from: data) else {
            isLoading = false
            showJSONError()
            return
        }
        
        model.data = response
        
        getAllProducts(of: response)
        delegate?.reloadData()
        isLoading = false
    }
    
    private func getAllProducts(of data : Collections){
        data.forEach {[weak self] (collection) in
            for object in collection.products {
                self?.model.products.append(object.value)
            }
        }
    }
    
    private func showJSONError(){
        alertModel = AlertModel(title:"alert_error_title".localizable,
                                message: "alert_error_reading_json".localizable,
                                style: .alert,
                                actions: [])
    }
    
}

extension FavoritesViewModel{
    func getCollectionsCount() -> Int{
        return model.data.count
    }
    
    func getCollection(at indexPath : IndexPath) -> Collection{
        return model.data[indexPath.row]
    }
    
    func getProductsCount() -> Int{
        return model.products.count
    }
    
    func getProduct(at indexPath : IndexPath) -> Product{
        return model.products[indexPath.row]
    }
}

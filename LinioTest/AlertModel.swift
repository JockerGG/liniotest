//
//  AlertOption.swift
//  LinioTest
//
//  Created by Eduardo García González on 12/16/18.
//  Copyright © 2018 Eduardo García González. All rights reserved.
//

import Foundation
import UIKit

struct AlertModel{
    var title : String
    var message : String
    var style : UIAlertController.Style
    var actions : [UIAlertAction]
}

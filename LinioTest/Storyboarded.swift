//
//  Storyboarded.swift
//  LinioTest
//
//  Created by Eduardo García González on 12/15/18.
//  Copyright © 2018 Eduardo García González. All rights reserved.
//

import Foundation
import UIKit

protocol Storyboarded {
    /**
     instantiate a view controller with that identifier, and force cast as the type that was requested
     */
    static func instantiate() -> Self
}

extension Storyboarded where Self: UIViewController {
    static func instantiate() -> Self {
        /**
         The full name of view controller. Example: MyApp.ViewController
         */
        let fullName = NSStringFromClass(self)
        
        /**
         This splits by the dot and uses everything after, giving "MyViewController"
         */
        let className = fullName.components(separatedBy: ".")[1]
        
        /**
            This is the storyboard where is located the view controller
         */
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        return storyboard.instantiateViewController(withIdentifier: className) as! Self
    }
}

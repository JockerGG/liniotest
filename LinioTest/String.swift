//
//  Strings.swift
//  LinioTest
//
//  Created by Eduardo García González on 12/16/18.
//  Copyright © 2018 Eduardo García González. All rights reserved.
//

import Foundation

extension String{
    var localizable : String {
        get{
            return NSLocalizedString(self, comment: "")
        }
    }
}

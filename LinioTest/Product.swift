//
//  Product.swift
//  LinioTest
//
//  Created by Eduardo García González on 12/16/18.
//  Copyright © 2018 Eduardo García González. All rights reserved.
//

import Foundation

struct Product: Codable{
    var id : Int
    var name : String
    var wishListPrice : Int
    var slug : String
    var url : String
    var image : String
    var linioPlusLevel : Int
    var conditionType : ProductCondition
    var freeShipping : Bool
    var imported : Bool
    var active: Bool
}

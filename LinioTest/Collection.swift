//
//  Collection.swift
//  LinioTest
//
//  Created by Eduardo García González on 12/16/18.
//  Copyright © 2018 Eduardo García González. All rights reserved.
//

import Foundation

struct Collection : Codable{
    var id : Int
    var name : String
    var description : String
    var `default` : Bool
    var owner : Owner
    var createdAt : String
    var visibility : String
    var products : [String : Product]
}

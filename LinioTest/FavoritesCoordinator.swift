//
//  FavoritesCoordinator.swift
//  LinioTest
//
//  Created by Eduardo García González on 12/15/18.
//  Copyright © 2018 Eduardo García González. All rights reserved.
//

import Foundation
import UIKit

class FavoritesCoordinator: Coordinator {
    /**
        The navigation controller is necessary to push the view controllers.
     */
    var navigationController: UINavigationController
    
    /**
     Initialization of FavoritesCoordinator
     - parameters:
        - navigationController: Is required to allow push view controllers.
     */
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        let vc = FavoritesViewController.instantiate()
        navigationController.pushViewController(vc, animated: false)
    }
}

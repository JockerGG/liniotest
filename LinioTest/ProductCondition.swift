//
//  ProductCondition.swift
//  LinioTest
//
//  Created by Eduardo García González on 12/16/18.
//  Copyright © 2018 Eduardo García González. All rights reserved.
//

import Foundation

enum ProductCondition : String, Codable{
    case refurbished
    case new
}

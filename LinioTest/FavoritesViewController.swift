//
//  ViewController.swift
//  LinioTest
//
//  Created by Eduardo García González on 12/15/18.
//  Copyright © 2018 Eduardo García González. All rights reserved.
//

import UIKit

class FavoritesViewController: UIViewController, Storyboarded {
    @IBOutlet weak var collectionView: UICollectionView!{
        didSet{
            collectionView.register(UINib(nibName: "CollectionCell", bundle: nil), forCellWithReuseIdentifier: "collection")
            collectionView.register(UINib(nibName: "ProductCell", bundle: nil), forCellWithReuseIdentifier: "product")
            collectionView.register(UINib(nibName: "SectionHeader", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "section")
            collectionView.collectionViewLayout.invalidateLayout()
            let layout = GridFlowLayout()
            layout.headerReferenceSize = CGSize(width: collectionView.frame.width, height: 30)
            layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 32, right: 0)
            collectionView.showsVerticalScrollIndicator = false
            collectionView.setCollectionViewLayout(layout, animated: true)
            collectionView.dataSource = self
            collectionView.delegate = self
            collectionView.addSubview(refreshControl)
        }
    }
    
    lazy var refreshControl : UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(reload(_:)), for: .valueChanged)
        refreshControl.tintColor = .blue
        return refreshControl
    }()
    
    lazy var vm : FavoritesViewModel? = {
        return FavoritesViewModel()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initViews()
        initVM()
    }

    func initViews(){
        self.setTitle(title: "favorites_title".localizable, color: .black)
        self.view.backgroundColor = .whiteTwo
        self.collectionView.backgroundColor = .whiteTwo
    }
    
    func initVM(){
        vm?.delegate = self
        vm?.readAndDecodeJSON()
    }

}

extension FavoritesViewController : FavoritesViewModelDelegate{
    func showAlert(with model: AlertModel) {
        self.displayAlert(withAlertModel: model)
    }
    
    func reloadData() {
        collectionView.reloadData()
    }
    
    func updateLoader(isLoading: Bool) {
        isLoading ? refreshControl.beginRefreshing() : refreshControl.endRefreshing()
    }
    
    @objc func reload(_ sender : UIRefreshControl){
        vm?.readAndDecodeJSON()
    }
}

extension FavoritesViewController : UICollectionViewDataSource, UICollectionViewDelegate{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return section == 0 ? (vm?.getCollectionsCount() ?? 0) : (vm?.getProductsCount() ?? 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return indexPath.section == 0 ? createCollectionCell(collectionView: collectionView, indexPath: indexPath) : createProductCell(collectionView: collectionView, indexPath: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        guard let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "section", for: indexPath) as? SectionHeader else {
            fatalError()
        }
        header.title.text = indexPath.section == 0 ? "" : String(format: "all_favorites_title".localizable, (vm?.getProductsCount() ?? 0).description)
        return header
    }
    
    func createCollectionCell(collectionView :  UICollectionView, indexPath : IndexPath) -> CollectionCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collection", for: indexPath) as? CollectionCell else {
            fatalError("Cell cannot be initialized")
        }
        
        guard let model = vm?.getCollection(at: indexPath) else {
            fatalError("Model not found")
        }
        
        cell.configureWith(model: model)
        return cell
    }
    
    func createProductCell(collectionView :  UICollectionView, indexPath : IndexPath) -> ProductCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "product", for: indexPath) as? ProductCell else {
            fatalError("Cell cannot be initialized")
        }
        
        guard let model = vm?.getProduct(at: indexPath) else {
            fatalError("Model not found")
        }
        
        cell.configure(with: model)
        
        return cell
    }
    
}

//
//  ProductCell.swift
//  LinioTest
//
//  Created by Eduardo García González on 12/16/18.
//  Copyright © 2018 Eduardo García González. All rights reserved.
//

import UIKit

class ProductCell: UICollectionViewCell {
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var levelImage: UIImageView!
    @IBOutlet weak var productTypeImage: UIImageView!
    @IBOutlet weak var truckConstant: NSLayoutConstraint!
    @IBOutlet weak var travelConstant: NSLayoutConstraint!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var badgesView: UIView!
    
    override func awakeFromNib() {
        prepareCell()
    }
    
    override func prepareForReuse() {
        prepareCell()
    }
    
    private func prepareCell(){
        self.setViewCornerRadius(radius: 5)
        badgesView.roundTopLeftCorners(radius: 7)
        truckConstant.constant = 0
        travelConstant.constant = 0
        self.layoutIfNeeded()
        activityIndicator.startAnimating()
        activityIndicator.isHidden = false
    }
    
    func configure(with model : Product){
        activityIndicator.startAnimating()
        activityIndicator.isHidden = false
        levelImage.image = UIImage(named: model.linioPlusLevel == 1 ? "ndIc30PlusSquare" : "ndIc30Plus48Square")
        productTypeImage.image = UIImage(named: model.conditionType == .refurbished ? "ndIc30RefurbishedSquare" : "ndIc30NewSquare")
        truckConstant.constant = model.freeShipping ? 29 : 0
        travelConstant.constant = model.imported ? 29 : 0
        UIView.animate(withDuration: 0.2) {[weak self] in
            self?.layoutIfNeeded()
        }
        
        guard let url = URL(string: model.image) else {
            updateIndicator(isHidden: true)
            return
        }
        
        guard let data = try? Data(contentsOf: url) else {
            updateIndicator(isHidden: true)
            return
        }
        
        guard let image = UIImage(data: data) else {
            updateIndicator(isHidden: true)
            return
        }
        
        productImage.image = image
        updateIndicator(isHidden: true)
        
    }
    
    func updateIndicator(isHidden : Bool){
        activityIndicator.isHidden = isHidden
        isHidden ? activityIndicator.stopAnimating() : activityIndicator.startAnimating()
    }
}

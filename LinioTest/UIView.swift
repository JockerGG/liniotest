//
//  UIView.swift
//  LinioTest
//
//  Created by Eduardo García González on 12/16/18.
//  Copyright © 2018 Eduardo García González. All rights reserved.
//

import Foundation
import UIKit

extension UIView{
    func setViewCornerRadius(radius : CGFloat){
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = true
    }

    func roundTopLeftCorners(radius : CGFloat){
        self.clipsToBounds = true
        if #available(iOS 11.0, *) {
            self.layer.cornerRadius = radius
            self.layer.maskedCorners = [.layerMinXMinYCorner]
        } else {
            let maskPAth1 = UIBezierPath(roundedRect: self.frame,
                                         byRoundingCorners: ([.topRight , .topLeft]),
                                         cornerRadii:CGSize(width: radius, height: radius))
            let maskLayer1 = CAShapeLayer()
            maskLayer1.frame = self.bounds
            maskLayer1.path = maskPAth1.cgPath
            self.layer.mask = maskLayer1
        }
    }
    
    func roundTopCorners(radius : CGFloat){
        self.clipsToBounds = true
        if #available(iOS 11.0, *) {
            self.layer.cornerRadius = radius
            self.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        } else {
            let maskPAth1 = UIBezierPath(roundedRect: self.frame,
                                         byRoundingCorners: ([.topRight , .topLeft]),
                                         cornerRadii:CGSize(width: radius, height: radius))
            let maskLayer1 = CAShapeLayer()
            maskLayer1.frame = self.bounds
            maskLayer1.path = maskPAth1.cgPath
            self.layer.mask = maskLayer1
        }
    }
    
    func roundedBottomCorners(radius : CGFloat, clip : Bool){
        self.clipsToBounds = clip
        if #available(iOS 11.0, *) {
            self.layer.cornerRadius = radius
            self.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        } else {
            let maskPAth1 = UIBezierPath(roundedRect: self.bounds,
                                         byRoundingCorners: [.bottomLeft , .bottomRight],
                                         cornerRadii:CGSize(width: radius, height: radius))
            let maskLayer1 = CAShapeLayer()
            maskLayer1.frame = self.bounds
            maskLayer1.path = maskPAth1.cgPath
            self.layer.mask = maskLayer1
        }
    }
}

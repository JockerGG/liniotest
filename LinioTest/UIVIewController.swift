//
//  Extensions.swift
//  LinioTest
//
//  Created by Eduardo García González on 12/16/18.
//  Copyright © 2018 Eduardo García González. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController{
    func displayAlert(withAlertModel model : AlertModel){
        let defaultAction = UIAlertAction(title: "default_action".localizable, style: .default, handler: nil)
        let alert = UIAlertController(title: model.title, message: model.message, preferredStyle: model.style)
        let actions = model.actions.count == 0 ? [defaultAction] : model.actions
        
        actions.forEach { (action) in
            alert.addAction(action)
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    func setTitle(title : String, color : UIColor){
        self.navigationController?.navigationBar.titleTextAttributes =
            [NSAttributedString.Key.foregroundColor: color,
             NSAttributedString.Key.font: UIFont.systemFont(ofSize: 22, weight: .bold)]
        
        self.title = title
    }
}


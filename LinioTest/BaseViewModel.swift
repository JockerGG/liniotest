//
//  BaseViewModel.swift
//  LinioTest
//
//  Created by Eduardo García González on 12/16/18.
//  Copyright © 2018 Eduardo García González. All rights reserved.
//

import Foundation

protocol BaseViewModel {
    /**
    Indicate to the view what is necessary display an alert.
     - Parameter model: AlertModel declared in AlertModel.swift
     
     */
    func showAlert(with model : AlertModel)
    
    
    func updateLoader(isLoading : Bool)
    
    /**
     Indicate to the view what is necessary reload data of collection or table view.
     
     This function is initialized because isn't necessary always.
     */
    func reloadData()
}

extension BaseViewModel{
    func reloadData(){}
}
